FROM openjdk:11-jre-slim
ADD *.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]