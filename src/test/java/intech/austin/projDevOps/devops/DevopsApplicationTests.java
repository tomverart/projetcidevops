package intech.austin.projDevOps.devops;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DevopsApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void addition(){
		int a = 2;
		int b = 3;

		DevopsApplication calcul = new DevopsApplication();

		int somme = calcul.addition(a,b);

		assertEquals(5, somme);
	}

}
