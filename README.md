projet CI / CD

Initialisation d'une application "hello world" avec spring boot

Automatisation du packaging après un commit et mise à disposition du JAR au téléchargement

J'ai tenté de mettre en place une image docker, j'ai suivi beaucoup de docs, mais ce que j'ai fait me laisser dubitatif (ca ne plante plus, donc ca doit faire quelque chose, ou bien rien du tout.)
si possible, j'aimerais que nous prennions 10/15mn pour voir comment cela marche réellement et que tu puisse m'expliquer (je me suis perdu dans la doc de gitlab et docker et j'avoue ne pas trop comprendre ce sur quoi je tombe)


Conclusion :
j'ai bien le concept d'intégration, avec le packaging et le jar dispo en téléchargement.

La partie déploiement a besoin d'être renforcé/réexpliqué/approfondie, car dans le fond, "ca n'a pas l'air si compliqué", juste besoin d'en faire quelques-uns pour saisir le concept. 
C'est un sujet qui m'intéresse vraiment et j'ai envie de comprendre pour pouvoir me l'approprier pour pouvoir m'en resservir à l'avenir.
